
/**
 * Given a string, find the length of the longest substring without repeating characters. 
 * For example, the longest substring without repeating letters for "abcabcbb" is "abc", which 
 * the length is 3. For "bbbbb" the longest substring is "b", with the length of 1.
 */

exports = module.exports = {};

/*
 * @param {string} s
 * @return {number}
 */
exports.lengthOfLongestSubstring = function lengthOfLongestSubstring(s) {
    var currentDict = {};
    var stringLength = 0, maxLength = 0;
    
    var startIndex = 0, endIndex = 0;

    var duplicateChar = null;
    
    while (endIndex < s.length && startIndex <= endIndex) {
        // if there are duplicates, increase startIndex and pop off values in dictionary until there are no more duplicates
        // otherwise increase endIndex, add values to dictionary and check for duplicates
        
        if (duplicateChar !== null) {
            // Turn off duplicate character if startIndex matches
            if (s[startIndex] === duplicateChar) {
                duplicateChar = null;
            }
            
            // Remove value from dictionary
            currentDict[s[startIndex]] = null;
            startIndex++;
            stringLength--;
        } else {
            if (typeof currentDict[s[endIndex]] !== 'undefined' &&
                       currentDict[s[endIndex]] !== null) {
                           
                // if duplicate set mark duplicate
                duplicateChar = s[endIndex];
            } else {
                currentDict[s[endIndex]] = true;
                endIndex++;
                stringLength++;
                if (stringLength > maxLength) {
                    maxLength = stringLength;
                }
            }
        }
    }
    
    if (stringLength > maxLength) {
        maxLength = stringLength;
    }
    
    return maxLength;
};

