
/**
 * Reverse digits of an integer. 
 *
 * Example1: x = 123, return 321 
 * Example2: x = -123, return -321
 */

exports = module.exports = {};

/**
 * @param {number} x
 * @return {number}
 */
exports.reverse = function reverse(x) {
    var result = 0;
    
    // Example for 123:
    // result = 0 + 3 = 3
    // result *= 10 = 30
    // result = 30 + 2 = 32
    // result *= 10 = 320
    // result = 320 + 1 = 321
    // don't multiply by 10 at the end because x becomes 0
    
    while (x !== 0) {
        var digit = x % 10;
        
        // add current digit
        result += digit;

        // Properly handle both positive and negative numbers
        x = (x >= 0) ? Math.floor(x / 10) : Math.ceil(x / 10);
        
        // multiply by 10 if not at the end
        if (x !== 0) {
            result *= 10;
        }
    }
    
    // Stupid overflow check even though it doesn't overflow in Javascript
    if (result >= Math.pow(2, 31) - 1 ||
        result < -Math.pow(2, 31) + 1) {

        return 0;
    }
    
    return result;
};

