
/**
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', 
 * determine if the input string is valid.
 * The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
 */

exports = module.exports = {};

/**
 * @param {string} s
 * @return {boolean}
 */
exports.isValid = function isValid(s) {
    var opStack = [];
    
    for (var i = 0; i < s.length; i++) {
        switch (s[i]) {
            case '(':
            case '{':
            case '[':
                // push to stack for starting characters
                opStack.push(s[i]);
                break;
                
            case ')':
            case '}':
            case ']':
                var startingChar = opStack.pop();
                // check if the ending character matches the starting character from the top
                // of the stack
                if (!((s[i] === ')' && startingChar === '(') ||
                      (s[i] === '}' && startingChar === '{') ||
                      (s[i] === ']' && startingChar === '['))) {
                    
                    return false;            
                }
                break;
        }
    }

    return opStack.length === 0;
};

