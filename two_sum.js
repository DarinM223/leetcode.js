
/**
 * Given an array of integers, find two numbers such that they add up to a specific target number.
 *
 * The function twoSum should return indices of the two numbers such that they add up to the target,
 * where index1 must be less than index2. Please note that your returned answers (both index1 and index2) 
 * are not zero-based.
 *
 * You may assume that each input would have exactly one solution.
 *
 * Input: numbers={2, 7, 11, 15}, target=9
 * Output: index1=1, index2=2
 */

exports = module.exports = {};

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
exports.twoSum = function twoSum(nums, target) {
    var numberMap = {};
    
    var i;
    
    // Store value -> key in a hashmap
    for (i = 0; i < nums.length; i++) {
        numberMap[nums[i]] = i;
    }
    
    for (i = 0; i < nums.length; i++) {
        // Calculate target - value1 to get value2 and check if it is in the hashmap
        // and its index is greater than value1's index
        var otherValue = target - nums[i];
        if (typeof numberMap[otherValue] !== 'undefined' && 
                   numberMap[otherValue] !== i &&
                   numberMap[otherValue] > i) {
            return [i+1, numberMap[otherValue]+1]; 
        }
    }
    
    return null;
};

